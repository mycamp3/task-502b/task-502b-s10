import { Car } from "./car.js";
import { Retangle } from "./retangle.js";
import { Square } from "./square.js";
var car1 = new Car("Toyota", 2020);
console.log(car1._name);
console.log(car1._year);
console.log(car1.getAge());
var car2 = new Car();


var retangle1 = new Retangle(5, 10);
console.log((retangle1._width));
console.log(retangle1._height);

var square1 = new Square (5);
console.log(square1._width);
console.log(square1._height);
console.log(square1.getPerimeter());

console.log(retangle1 instanceof Retangle);

// hoisting: chỉ cần khai báo, trước hay sau câu lệnh đều đc
// Class ko hoisting
i = 0;
i = i + 1;
console.log(i);
var i;