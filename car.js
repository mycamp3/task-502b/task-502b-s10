class Car {
    _name;
    _year;

    constructor(paraName, paramYear) { // phương thức khởi tạo
        this._name = paraName;
        this._year = paramYear;
    }

    getAge() {
        var today = new Date();
        return today.getFullYear() - this._year;
    }
}

export { Car};
